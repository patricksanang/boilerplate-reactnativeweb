That is a boilerplate for those who want to build an app which could have either a web or a native version (android and ios) in React Native and React Native Web.

The technologies used and integrated are:

- React Native
- React Native Web
- Apollo client to make graphql queries to a remote server
- React-native-i18n for internationalization
- Expo


The React Native App made by iGamers for the katikas partners
