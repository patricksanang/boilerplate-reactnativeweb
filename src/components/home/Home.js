import React from 'react'
import PropTypes from 'prop-types'
import { View, Text } from 'react-native';

class Home extends React.PureComponent {
  render () {
    return (
      <View>
       <Text>Hello world!</Text>
     </View>
    )
  }
}

Home.propTypes = {
  articles: PropTypes.any,
}

export default Home;
