export {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from 'react-router-dom'

export {
  Redirect
} from 'react-router'
