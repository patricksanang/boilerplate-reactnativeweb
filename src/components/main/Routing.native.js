export {
  NativeRouter as Router, // Rename
  Switch,
  Route,
  Redirect,
  Link
} from 'react-router-native'
