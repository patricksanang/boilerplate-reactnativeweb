import React from 'react'
import PropTypes from 'prop-types';
import Home from '../home/Home'
import Dashboard from '../dashboard/Dashboard'
import { Router, Redirect, Switch, Route, Link } from './Routing'
import { token, account, BACKEND_URL } from '../../export'

class RouteTest extends React.PureComponent{
  static propTypes = {
    path: PropTypes.any,
    component: PropTypes.any,
  }
  isAuthenticated() {
    return true; //TODO: to be updated with the right value in the store of apollo-client
  }

  render () {
    const { path, component } = this.props
    return (
      this.isAuthenticated() ? (
        <Route path={path} component={component} />
      ) : (
        <Redirect to='/' />
      )
    )
  }
}

class Main extends React.PureComponent {
  render () {
    return (
      <Router>
        <Switch>
          <Route exact path='/' component={Home} />
          <Route exact path='/home' component={Home} />
          <RouteTest path='/dashboard' component={Dashboard} />
        </Switch>
      </Router>
    )
  }
}
export default Main
