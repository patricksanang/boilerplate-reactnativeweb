GLOBAL.self = GLOBAL; // to resolve the problem of fetch from isomorphic

import React from 'react';
import { AppRegistry } from 'react-native';

//apollo imports
import { ApolloClient } from 'apollo-client';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { HttpLink } from 'apollo-link-http';
import { onError } from 'apollo-link-error';
import { ApolloLink } from 'apollo-link';
import { ApolloProvider } from "react-apollo";

//apollo queueing
import { RetryLink } from 'apollo-link-retry';
import QueueLink from 'apollo-link-queue';

import cache from './cachepersist'

import Main from './components/main/Main';
import {URI_GRAPHQL} from './export';

const offlineLink = new QueueLink();
const retryLink = new RetryLink();

//window.offlineLink = offlineLink
//window.retryLink = retryLink

// Note: remove these listeners when your app is shut down to avoid leaking listeners.
//global.addEventListener('offline', () => offlineLink.close());
//global.addEventListener('online', () => offlineLink.open());

const client = new ApolloClient({
  link: ApolloLink.from([
    onError(({ graphQLErrors, networkError }) => {
      if (graphQLErrors)
        graphQLErrors.map(({ message, locations, path }) =>
          console.log(
            `[GraphQL error]: Message: ${message}, Location: ${locations}, Path: ${path}`,
          ),
        );
      if (networkError) console.log(`[Network error]: ${networkError}`);
    }),
    retryLink, //retry when connection will be established
    offlineLink, //for queuing in offline mode
    new HttpLink({
      uri: URI_GRAPHQL,
    }),
  ]),
  cache: cache
});

class App extends React.PureComponent {
  render () {
    return (
      <ApolloProvider client={client}>
        <Main />
      </ApolloProvider>
    )
  }
}

AppRegistry.registerComponent('HybridApp', () => App);
export default App
