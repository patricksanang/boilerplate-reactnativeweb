export const token = 'TOKEN'
export const account = 'account'

export const BACKEND_URL = 'http://127.0.0.1:8000/api/'
//export const BACKEND_URL = 'https://blucashfp.herokuapp.com/api/'
export const BACKEND_GATEWAY_URL = 'https://blucash.herokuapp.com/api/endpoint/'

export const URI_GRAPHQL = 'https://w5xlvm3vzz.lp.gql.zone/graphql'
